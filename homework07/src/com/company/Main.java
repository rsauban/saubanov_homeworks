package com.company;

import java.util.Scanner;

public class Main {

    public static void getMinCount() {
        Scanner scanner = new Scanner(System.in);
        // считываем первое число
        int num = scanner.nextInt();
        // если оно удовлетворяет условиям то начинаем вычисления
        if (num != -1 && -100 <= num && num <= 100) {
            // Создаем массив где в качестве значений хранится количество раз,
            // сколько встречается число индекс в последовательности.
            // диапазон [0,201] == [-100,100] так как тип индекса в java дб больше 0
            int arrCount[] = new int[201];
            // инициализируем массив нулями, хотя возможно там без присвоения уже нули
            for (int i = 0; i < arrCount.length; i++){
                arrCount[i]=0;
            }
            //  наибольшее количество раз вхождения числа в последовательность
            //  из условия задачи любое число может встречаются не более 2^32 раз а это максимальное число типа integer
            int minCount = Integer.MAX_VALUE;
            // если число удовлетворяет условиям
            while (num != -1 && -100 <= num && num <= 100) {
                // то +1 в ячеку под номером [число + 100]
                arrCount[num + 100]++;
                // считываем следующие число
                num = scanner.nextInt();
            }

            //  проходим весь массив ищем минимальный элемент. Поиск O(n)
            for (int i = 0; i < arrCount.length; i++){
                // искомое число должно встречаться больше 0 раз
               if ( arrCount[i] > 0 && arrCount[i] < minCount) {
                   // минимальный элемент
                   minCount = arrCount[i];
               }

            }

            // вывод чисел с минимальным количеством вхождений в последовательность
            for (int i = 0; i < arrCount.length; i++) {
                if ( minCount == arrCount[i] ) {
                    System.out.println(i - 100);
                }
            }
        } else {
           System.out.println("Ошибка! Число:" + num + " не удовлетворяет условию.");
        }

    }
    public static void main(String[] args) {
        getMinCount();

    }
}
